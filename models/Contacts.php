<?php

namespace app\models;

use Yii;

class Contacts extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'contacts';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'tel'], 'string', 'max' => 255],
            [['email'], 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'tel' => 'Тел.',
            'email' => 'Email',
        ];
    }
}
