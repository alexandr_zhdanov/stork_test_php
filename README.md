DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


УСТАНОВКА
------------

### Установка через composer

Если не установлен [Composer](http://getcomposer.org/), установите его по следующей инструкции
[getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).
Затем последовательно выполняем следующие команды:
~~~
sudo apt-get install php-mbstring
sudo apt-get install phpunit
~~~

And

~~~
sudo apt-get install php-sqlite3
~~~

После установки выполните следуюшие команды:

~~~
php composer.phar update
~~~

or

~~~
composer update
~~~

Запускаем сервер

~~~
php yii serve
~~~

Перейдите по ссылке [stork-test](http://localhost:8080/)
